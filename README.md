### About

- Web app: wrote in React with Redux
- Api: wrote in NodeJs Express
- DB: Mysql
- POSTMAN collection

### Features
- Web app: login and sign up
- Api: login, signup endpoints, plus change name endpoint

### RUN (requiremen docker)
- docker-compose up

- web app will run on localhost:3000
- api will run on localhost:5000
