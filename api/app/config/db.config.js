module.exports = {
  HOST: "mysqldb",
  USER: "root",
  PASSWORD: "pwd",
  DB: "testdb",
  dialect: "mysql",
  PORT: "3306",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
};