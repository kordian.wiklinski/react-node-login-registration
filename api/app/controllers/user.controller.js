const db = require("../models");
const User = db.user;
const Op = db.Sequelize.Op;

exports.allAccess = (req, res) => {
    res.status(200).send("Public Content.");
};

exports.userBoard = (req, res) => {
    res.status(200).send("User Content.");
};

exports.updatename = (req, res) => {
    const { username, newusername } = req.body;

    if (!username || !newusername) {
        res.status(400).send({ message: 'No username provided' });
    }

    User.findOne({
        where: {
            username: username
        }
    })
        .then(user => {
            if (!user) {
                return res.status(404).send({ message: "User Not found." });
            }
            // Check if record exists in db
            console.log('selected user', user);
            user.update({
                username: newusername
            }).then((user) => {
                res.status(200).send({
                    ...user,
                    username: user.username,
                });
            })
        }).catch(err => {
            res.status(400).send({ message: `Failed update username provided ${err.message}` });
        })
}